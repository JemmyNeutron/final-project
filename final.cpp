#include "7edar.hpp"
#include "jemmy.hpp"
#include "amr.hpp"
#include "yasser.hpp"


using std::cin;
using std::cout;
using std::endl;
using std::string;
int main()
{
    client::client_attributes test;

    int choice;
    cout <<"\n\n";
    cout << "                          1-Create new account\n"
         << endl;
    cout << "                          2-Search existing account\n"
         << endl;
    cout << "                          3-take some money\n"
         << endl;    
    cout << "                          4-add some money\n\n"
         << endl;
    cout << "           ###### Please enter your choise (1 or 2 or 3 or 4) ######\n\n" << endl;
    cout << "Your choice:- ";
    cin >> choice;

    if (choice == 1)
        INPUT(test);
    else if (choice == 2)
        SEARCH(test);
    else if (choice == 3)
        PULL(test);
    else if (choice == 4)
        PUSH(test);
    return main();
}