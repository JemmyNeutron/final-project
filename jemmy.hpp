#ifndef jemmy_HPP
#define jemmy_HPP
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iomanip>
using std::cin;
using std::cout;
using std::endl;
using std::string;
namespace client
{
struct client_attributes
{
	string name;
	string ID;
	int age;
	double salary;
};
void INPUT(client_attributes add)
{
	add.name = "";
	add.ID = "";
	add.age = 0;
	add.salary = 0;

	std::fstream input;

	input.open("clients_data.txt", std::ios::out | std::ios::app);
    cout << "\n";
	cout << "please enter your ID" << endl;
	input << "\n";
	cin >> add.ID;
	input << add.ID;

	cout << "please enter your name" << endl;
	cin >> add.name;
	input << "\t" << add.name;

	cout << "please enter your age" << endl;
	cin >> add.age;
	input << "\t" << add.age;

	cout << "please enter your salary" << endl;
	cin >> add.salary;
	input << "\t" << add.salary;

	input.close();
	cout << "\n\n";
}
} // namespace client

#endif