#ifndef amr_HPP
#define amr_HPP
#include "jemmy.hpp"
namespace client
{
void SEARCH(client::client_attributes add)
{
	std::fstream input;
	bool found = false;
	string id_search;
	cout << "\n";
	cout << "Search by your ID" << endl;
	cin.ignore();
	cin >> id_search;

	input.open("clients_data.txt", std::ios::in);
	while (!input.eof())
	{

		input >> add.ID;
		if (add.ID == id_search)
		{
			input >> add.name >> add.age >> add.salary;
			cout << "Your name:- " << add.name << endl;
			cout << "Your age :- " << add.age << endl;
			cout << "Your salary:- " << add.salary << endl;
			found = true;
		}
	}
	input.close();

	if (found != true)
	{
		cout << "not found" << endl;
	}
	 cout << "\n\n";
}
} // namespace client
#endif